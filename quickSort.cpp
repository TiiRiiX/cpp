#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstdio>
using namespace std;

int a[10000000];

void quickSort(int l, int r)
{
    int x = a[l + (r - l) / 2];
    int i = l;
    int j = r;

    while(i <= j)
    {
        while(a[i] < x) i++;
        while(a[j] > x) j--;
        if(i <= j)
        {
            swap(a[i], a[j]);
            i++;
            j--;
        }
    }

    if (i<r) quickSort(i, r);
    if (l<j) quickSort(l, j);    
}
int main()
{
    int n = 10000000;

    for(int i = 0; i < n; i++) a[i] = rand();
		clock_t start;
		start = clock();
    quickSort(0, n-1);
		cout << n << " elements were sorted" << endl;
		cout << "time:" << (clock()-start)/(double)(CLOCKS_PER_SEC/1000) << " ms" << endl;     
    return 0;
}
