#include "stdafx.h"
#include <iostream>

using namespace std;

struct Node
{
	int x;//информационный элемент
	Node *Next;//указатель на следующий элемент
};
typedef Node *PNode;

void Add(int x, PNode &Head)//добавление элемента в стек
{
	PNode MyNode;
	if (Head == NULL)
	{
		Head = new(Node);
		MyNode = Head;
		Head->Next = NULL;
	}
	else
	{
		MyNode = new(Node);
		MyNode->Next = Head;
		Head = MyNode;
	}
	MyNode->x = x;
}
void Show(PNode &Head)//отображение стека
{
	PNode MyNode;
	MyNode = Head;//объявляем указатель на начало стека
	while (MyNode != NULL)//пока указатель на следующий элемент не NULL
	{
		cout << MyNode->x << " ";//выводим поле
		MyNode = MyNode->Next;//переходим к следующему элементу
	}
}
void ClearNode(PNode &Head)//удаление стека из памяти
{
	PNode MyNode;
	while (Head != NULL)//пока голова стека не указывает на NULL
	{
		MyNode = Head->Next;//переменная для хранения адреса следующего элемента
		delete Head;//освобождение адреса начала стека
		Head = MyNode;//меняем адрес начала стека
	}
}

void main()
{
	PNode Head;
	Head = NULL;
	for (int i = 0;i<10;i++) //заносим данные в стек
		Add(i, Head);
	Show(Head);//выводим стек
	ClearNode(Head); //очищаем память
}