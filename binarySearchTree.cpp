#include "stdafx.h"
#include <iostream>
#include <time.h>  
using namespace std;

struct Node {
	int data;
	Node *left, *right;
};
typedef Node *PNode;

int n = 10;
int tabs = 0;

void printTree(PNode p) {
	if (!p) return;
	tabs++;
	printTree(p->right);
	for (int i = 0; i < tabs; i++)
		cout << "      ";
	cout << p->data << endl;
	printTree(p->left);
	tabs--;
	return;
}

void addToTree(PNode &Tree, int data) {
	if (Tree == NULL) {
		Tree = new Node;
		Tree->data = data;
		Tree->left = NULL;
		Tree->right = NULL;
	}
	else {
		if (data < Tree->data)
			addToTree(Tree->left, data);
		if (data >= Tree->data)
			addToTree(Tree->right, data);
	}
}

void deleteTree(PNode p) {
	if (!p) return;
	deleteTree(p->right);
	deleteTree(p->left);
	delete p;
	return;
}

int main() {
	srand(time(NULL));
	PNode Tree = NULL;
	for (int index = 0; index < n; index++)
		addToTree(Tree, rand() % n);
	printTree(Tree);
	deleteTree(Tree);
	return 0;
}