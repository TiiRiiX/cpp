#include <iostream>
#include <cmath>
#include <ctime>
#include <cstdio>

int main(){
std::clock_t start;
start = std::clock();

double eps = pow(10.0, -25);
double x = 10.0;

x = x*M_PI/180;
double sin = 0.0;
double temp = x;
double i = 1;

while (fabs(temp) > eps) {
    sin += temp;
    temp *= -1 * x * x / ( 2 * double(i)) / (2 * double(i) + 1);
    i += 1;
}
std::cout << "sin(" << x << ") = " << sin << std::endl;
std::cout << "time:" << (std::clock()-start)/(double)(CLOCKS_PER_SEC/1000) << " ms" << std::endl;

printf("x");
}
