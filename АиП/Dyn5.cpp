#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <fstream>

using namespace std;

int a[30];
int target, countStay;

void poisk(int o) {
	if (o == target)
		countStay++;
	else if (o < target) {
		if (a[o + 1] != 1)
			poisk(o + 1);
		if (a[o + 2] != 1)
			poisk(o + 2);
		if (a[o + 3] != 1)
			poisk(o + 3);
		if (a[o + 4] != 1)
			poisk(o + 4);
	}
}

void Solve()
{
    Task("Dyn5");
	string inpath, outpath;
	pt >> inpath >> outpath;
	ifstream fin(inpath);
	ofstream fout(outpath);
	fin >> target;
	countStay = 0;
	int n = 0;
	while (!fin.eof()) {
		int t;
		fin >> t;
		a[t] = 1;
	}
	fin.close();
	poisk(0);
	fout << countStay;
	fout.close();
}
