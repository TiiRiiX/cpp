// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

void printMas(int *mas, int n){
	for (int i = 0; i < n; i++)
		cout << mas[i] << " ";
	cout << endl;
}

void createRandom(int *mas, int n, int a, int b){
	for (int i = 0; i < n; i++)
		mas[i] = a + rand() % b;
}

bool checkSimple(int a){
	a = abs(a);
	for (int i = 2; i < a; i++)
		if (a % i == 0) return false;
	if (a == 1) return false;
	return true;
}

void sortPuz(int *a, int n){
	for (int i = 0; i < n; i++){
		for (int j = n - 1; j > i; j--){
			 if (a[j] < a[j - 1])
            {
				int t = a[j];
				a[j] = a[j - 1];
				a[j - 1] = t;
			}
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	int A,B,N;
	cout << "Введите количество элементов: ";
	cin >> N;
	int *C = new int[N];
	int *D = new int[N];
	cout << "Введите границы интервала чисел: ";
	cin >> A >> B;
	createRandom(C, N, A, B);
	printMas(C, N);
	int countD = 0;
	int lastD = A;
	bool isGrow = true;
	for (int i = 0; i < N; i++)
		if (checkSimple(C[i])){
			D[countD] = C[i];
			if (lastD > D[countD])
				isGrow = false;
			else
				lastD = D[countD];
			countD++;
		}
	printMas(D, countD);
	if (isGrow)
		cout << "Это возрастающая последовательность" << endl;
	else{
		cout << "Это не возрастающая последовательность" << endl;
		sortPuz(D, countD);
	}
	printMas(D, countD);
	delete[] C;
	delete[] D;
	return 0;
}

