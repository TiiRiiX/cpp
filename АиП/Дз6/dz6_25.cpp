// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace std;

void printMas(int *mas, int n){
	for (int i = 0; i < n; i++)
		cout << mas[i] << " ";
	cout << endl;
}

int binaryFind(int *a, int tar, int left, int right, int n){
	if (left + 1 >= n || a[left + 1] >= tar) return left + 1;
	int m = (left + right) / 2;
	if (m < n && m > -1 && a[m] < tar)
		return binaryFind(a, tar, m, right, n);
	return binaryFind(a, tar, left, m, n);
}

int _tmain(int argc, _TCHAR* argv[])
{

	setlocale(LC_ALL, "Russian");

	int A[100];
	int n = 0;

	ifstream fin("out.txt");

	while(!fin.eof()){
		fin >> A[n];
		n++;
	}
	n--;
	printMas(A, n);
	int target;
	cout << "Target : ";
	cin >> target;
	//if (binaryFind
	int find =  binaryFind(A, target, -1, n, n);
	cout << "Позиции : ";
	int countFind = 0;
	while (find < n && A[find] == target){
		cout << find << " ";
		countFind++;
		find =  binaryFind(A, target, find, n, n);
	}
	if (countFind == 0) cout << "Таких не найдено";
	cout << endl;
	cout << "Всего = " << countFind << endl;
	fin.close();
	return 0;
}

