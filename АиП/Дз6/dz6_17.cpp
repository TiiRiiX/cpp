// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace std;

void sortPuz(int *a, int n){
	for (int i = n; i >= 0; i--){
		for (int j = i; j < n; j++){
			 if (a[j] < a[j - 1])
            {
				int t = a[j];
				a[j] = a[j - 1];
				a[j - 1] = t;
			}
		}
	}
}

void printMas(int *mas, int n){
	for (int i = 0; i < n; i++)
		cout << mas[i] << " ";
	cout << endl;
}

int _tmain(int argc, _TCHAR* argv[])
{

	int A[100];
	int n = 0;

	ifstream fin("in.txt");
	ofstream fout("out.txt");

	while(!fin.eof()){
		fin >> A[n];
		n++;
	}
	printMas(A, n);
	sortPuz(A, n);
	printMas(A,n);
	for (int i = 0; i < n; i++)
		fout << A[i] << " ";
	fin.close();
	fout.close();
	return 0;
}

