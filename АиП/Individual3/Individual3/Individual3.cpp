#include "stdafx.h"
#include <iostream>
#include <string>
#include <fstream>
#include <queue>
#include <map>
using namespace std;

struct Node {
	string letters;
	int size;
	Node *left, *right;
};
typedef Node *PNode;

struct isLess
{
	bool operator() (const PNode& x, const PNode& y) const {
		return x->size > y->size;
	}
};

struct letterData {
	int count = 0;
	string code;
};

string LoadText(string path) {
	ifstream f(path);
	string text = "";
	string line;
	while (!f.eof()) {
		getline(f, line);
		text += line + '\n';
	}
	return text;
}

int FillLetters(map<char, letterData> &letters, string &text) {
	int counter = 0;
	for (auto iter = text.begin(); iter != text.end(); iter++) {
		if ((*iter) == '\n') {
			continue;
		}
		if ((*iter) == '#') {
			break;
		}
		letters[(*iter)].count++;
		counter++;
	}
	return counter;
}

void FillQueue(priority_queue<PNode, vector<PNode>, isLess> &myPrQueue, map<char, letterData> &letters) {
	for each (auto letter in letters)
	{
		PNode node = new Node();
		node->left = NULL;
		node->right = NULL;
		node->letters = letter.first;
		node->size = letter.second.count;
		myPrQueue.push(node);
	}
}

PNode BuildTreeFromQueue(priority_queue<PNode, vector<PNode>, isLess> &myPrQueue) {
	PNode first, second;

	while (myPrQueue.size() > 1)
	{
		first = myPrQueue.top();
		myPrQueue.pop();
		second = myPrQueue.top();
		myPrQueue.pop();

		PNode node = new Node();
		node->left = first;
		node->right = second;
		node->letters = first->letters + second->letters;
		node->size = first->size + second->size;

		myPrQueue.push(node);
	}

	return myPrQueue.top();
}

void ParseTreeNode(map<char, letterData> &letters, PNode node, string currentCode) {
	if (node->left != NULL) {
		ParseTreeNode(letters, node->right, currentCode + "1");
		ParseTreeNode(letters, node->left, currentCode + "0");
	}
	if (node->letters.length() == 1)
		letters[node->letters[0]].code = currentCode;
}

void SetCodeToSymbols(map<char, letterData> &letters, PNode node) {
	if (node->left != NULL) {
		ParseTreeNode(letters, node->left, "0");
		ParseTreeNode(letters, node->right, "1");
	}
	else {
		letters[node->letters[0]].code = "0";
	}
}

int GetShTextLength(string text, map<char, letterData> &letters) {
	cout << "text:\n";
	int length = 0;
	for each (char letter in text)
	{
		if (letter == '\n')
			continue;
		else if (letter == '#')
			break;
		length += letters[letter].code.length();
		cout << letters[letter].code;
	}
	cout << endl;
	cout << length << endl;
	return length;
}

int main()
{
	setlocale(LC_ALL, "Russian");
	priority_queue<PNode, vector<PNode>, isLess> myPrQueue;
	map<char, letterData> letters;

	string text = LoadText("input.txt");
	//getline(cin, text);
	cout  << "text:\n" << text << endl;
	int size = FillLetters(letters, text);
	FillQueue(myPrQueue, letters);
	
	PNode tree = BuildTreeFromQueue(myPrQueue);

	SetCodeToSymbols(letters, myPrQueue.top());

	cout << "letter code count" << endl;
	for (auto iter = letters.begin(); iter != letters.end(); iter++)
		cout << (*iter).first << "\t" << (*iter).second.code << "\t" << (*iter).second.count << endl;

	ofstream f("output.txt");
	f << GetShTextLength(text, letters);
}