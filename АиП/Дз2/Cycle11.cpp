#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Solve()
{
    Task("Cycle11");
	int n;
	pt >> n;
	double sum = 0;
	int i, a;
	for (i = 1, a = 2; i <= n ; i++, a*=2) sum += (2*i - 1)/double(a);
	pt << sum;
}
