#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Solve()
{
    Task("Two20");
	int n;
	pt >> n;
	int big = 0;
	while (n > 0){
		if (big < (n % 10)) big = n % 10;
		n /= 10;
	}
	pt << big;
}
