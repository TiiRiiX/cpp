#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <fstream>
using namespace std;

bool checkSimple(int a){
	for (int i = 2; i < a; i++)
		if (a % i == 0) return false;
	if (a == 1) return false;
	return true;
}

void Solve()
{
    Task("Four13");
	ifstream fin("input.dat");
	ofstream fout("rez.dat");
	int K;
	pt >> K;
	int new_out;
	int last_i = 0;
	int main[100];
	while(!fin.eof()){
		fin >> new_out;
		if (checkSimple(new_out)){
			main[last_i] = new_out;
			last_i++;
		}
	}
	for (int j = 0; j < last_i; j++){
			fout << main[j] << " ";
	}
	fout << endl;
	for (int i = 0; i < K; i++){
		for (int j = last_i; j > 0; j--){
			main[j] = main[j - 1];
		}
		//last_i--;
		main[i] = 0;
		for (int j = 0; j < last_i; j++){
			fout << main[j] << " ";
		}
		fout << endl;
	}
	fout.close();
	fin.close();
}
