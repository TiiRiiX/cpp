// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

void printMas(int *mas, int n){
	for (int i = 0; i < n; i++)
		cout << mas[i] << " ";
	cout << endl;
}

void createRandom(int *mas, int n, int a, int b){
	for (int i = 0; i < n; i++)
		mas[i] = a + rand() % b;
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	int a,b,N;
	cout << "Введите количество элементов: ";
	cin >> N;
	int *A = new int[2*N];
	cout << "Введите границы интервала чисел: ";
	cin >> a >> b;
	createRandom(A, N, a, b);
	printMas(A, N);
	int maxA = a - 1;
	int countMax = 0;
	for (int i = 0; i < N; i++){
		if (maxA < A[i]){
			maxA = A[i]; 
			countMax = 1;
		} else if (maxA == A[i]){
			countMax++;
		}
	}
	int i = N - 1;
	for (int j = N + countMax - 1; i >= 0; i--, j--){
		if (A[i] == maxA)
			A[j--] = 0;
		A[j] = A[i]; 
	}
	N += countMax;
	printMas(A, N);
	delete[] A;
	return 0;
}

