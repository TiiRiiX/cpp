#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <fstream>
using namespace std;

void Solve()
{
    Task("Four1");
	unsigned long long mainMas[60];
	int N;
	pt >> N;
	ofstream fout("result.tst", ios::out);
	mainMas[0] = 2;
	for (int i = 1; i < N; i++) mainMas[i] = mainMas[i - 1]*2;
	for (int i = 0; i + 1 < N; i += 2){
			unsigned long long t = mainMas[i];
			mainMas[i] = mainMas[i+1];
			mainMas[i+1] = t;
			fout << mainMas[i] << endl;
			fout << mainMas[i+1] << endl;
	}
	if (N % 2 == 1)
		fout << mainMas[N-1] << endl;
	fout.close();
}
