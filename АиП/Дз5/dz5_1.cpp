// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

void printMas(int *mas, int n){
	for (int i = 0; i < n; i++)
		cout << mas[i] << " ";
	cout << endl;
}

void createRandom(int *mas, int n, int a, int b){
	for (int i = 0; i < n; i++)
		mas[i] = a + rand() % b;
}

int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	int a,b,N;
	cout << "Введите количество элементов: ";
	cin >> N;
	int *A = new int[N];
	cout << "Введите границы интервала чисел: ";
	cin >> a >> b;
	createRandom(A, N, a, b);
	printMas(A, N);
	int i = 1;
	for (int j = 2; j < N; i++, j+=2){
		A[i] = A[j];
	}
	N = i;
	printMas(A, N);
	delete[] A;
	return 0;
}

