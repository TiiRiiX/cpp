#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

int depth;

void checkDepth(PNode Tree, int newDepth) {
	if (!Tree) return;
	if (depth < newDepth)
		depth = newDepth;
	checkDepth(Tree->Left, newDepth + 1);
	checkDepth(Tree->Right, newDepth + 1);
}

void Solve()
{
    Task("TreeWork14");
	PNode Tree;
	pt >> Tree;
	checkDepth(Tree, 0);
	pt << depth;
}
