// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <string>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	string main;
	getline(cin, main);
	int wordCounter = 0;
	int lastSpace = 1;
	while (lastSpace){
		if (main.find(" ",lastSpace) - lastSpace < 1)
			lastSpace++;
		else{
			wordCounter++;
			lastSpace = main.find(" ",lastSpace);
		}
	}
	cout << wordCounter << endl;
	return 0;
}

