// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	char main[30];
	cin >> main;
	int sum = main[0] - '0';
	for (int i = 1; i < strlen(main); i+=2){
		if (main[i] == '+')
			sum += main[i+1] - '0';
		else
			sum -= main[i+1] - '0';
	}
	cout << sum << endl;
	return 0;
}

