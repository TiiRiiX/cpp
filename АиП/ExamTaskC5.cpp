#include <windows.h>
#include <iostream>
#include <iomanip>
#pragma hdrstop
#include "pt4exam.h"

struct client {
	int year;
	int month;
	int hours;
	int id;
};

void Solve()
{
    Task("ExamTaskC5");
	int n;
	cin >> n;
	int *years = new int[11];
	for (int i = 0; i < 11;i++)
		years[i] = 0;
	for (int i = 0; i < n; i++) {
		client t;
		cin >> t.year >> t.month >> t.hours >> t.id;
		years[t.year - 2000] += t.hours;
	}
	int maxYear = 0, maxTime = 0;
	for (int i = 0; i < 11; i++)
		if (years[i] > maxTime) {
			maxTime = years[i];
			maxYear = 2000 + i;
		}
		else if (years[i] == maxTime)
			if (i + 2000 < maxYear)
				maxYear = i + 2000;
	cout << maxYear << endl;
	cout << maxTime;
}
