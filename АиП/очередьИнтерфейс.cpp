// cplus.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <queue>
#include <iterator>

using namespace std;

void print(queue<int> temp) {
	while (!temp.empty()) {
		cout << temp.front() << " ";
		temp.pop();
	}
}

int main()
{
	setlocale(LC_ALL, "rus");
	queue <int> main;
	while (true) {
		cout << "Выберете действие: " << endl;
		cout << "1.Добавить элемент в очередь" << endl;
		cout << "2.Удалить элемент из очереди" << endl;
		cout << "3.Вывести первый элемент очереди" << endl;
		cout << "4.Вывести последний элемент очереди" << endl;
		cout << "5.Вывести содержимое очереди" << endl;
		cout << "6.Вывести количество элементов" << endl;
		cout << "7.Выход" << endl;
		cout << "Ввод: ";
		int choose;
		cin >> choose;
		switch (choose)
		{
		case 1:
			cout << "Введите новый элемент (целое число): ";
			int t;
			cin >> t;
			main.push(t);
			cout << t << " успешно добавлено" << endl;
			break;
		case 2:
			cout << "Удаляем элемент из очереди..." << endl;
			main.pop();
			cout << "Успешно" << endl;
			break;
		case 3:
			cout << "Первый элемент очереди: " << main.front();
			break;
		case 4:
			cout << "Последний элемент очереди: " << main.back();
			break;
		case 5:
			cout << "Вывод всей очереди..." << endl;
			print(main);
			cout << endl;
			break;
		case 6:
			cout << "Количество всех элементов очереди: " << main.size() << endl;
			break;
		case 7:
			cout << "Выход" << endl;
			return 0;
		default:
			cout << "Неверный ввод, повторите попытку" << endl;
			break;
		}
	}
    return 0;
}

