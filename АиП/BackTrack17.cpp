#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <fstream>
#include <iostream>
using namespace std;

int n, xc, yc, xb, yb, minPathCount;
char **pool;
string minPath;

void collectPath(int x, int y) {
	int tx = x;
	int ty = y;
	minPath = "";
	while (tx != xb || ty != yb) {
		if (pool[tx][ty] == 'D') {
			minPath = 'D' + minPath;
			tx--;
		}
		else if (pool[tx][ty] == 'U') {
			minPath = 'U' + minPath;
			tx++;
		}
		else if (pool[tx][ty] == 'R') {
			minPath = 'R' + minPath;
			ty--;
		}
		else if (pool[tx][ty] == 'L') {
			minPath = 'L' + minPath;
			ty++;
		}
	}
}

void Step(int x, int y, int depth) {
	if (x != 0 && x != n - 1 && y != 0 && y != n - 1) {
		if (x != xc || y != yc) {
			if (pool[x - 1][y] == ' ') {
				pool[x - 1][y] = 'U';
				Step(x - 1, y, depth + 1);
				pool[x - 1][y] = ' ';
			}
			if (pool[x + 1][y] == ' ') {
				pool[x + 1][y] = 'D';
				Step(x + 1, y, depth + 1);
				pool[x + 1][y] = ' ';
			}
			if (pool[x][y - 1] == ' ') {
				pool[x][y - 1] = 'L';
				Step(x, y - 1, depth + 1);
				pool[x][y - 1] = ' ';
			}
			if (pool[x][y + 1] == ' ') {
				pool[x][y + 1] = 'R';
				Step(x, y + 1, depth + 1);
				pool[x][y + 1] = ' ';
			}
		}
		else {
			if (minPathCount > depth)
			{
				minPathCount = depth;
				collectPath(x, y);
			}
		}
	}
}

void Solve()
{
    Task("BackTrack17");
	string inputFile;
	string s;
	pt >> inputFile;
	pt >> n;
	ifstream fin(inputFile);
	pool = new char *[n];
	for (int i = 0; i < n; i++)
		pool[i] = new char[n];
	cin.ignore();
	for (int i = 0; i < n; i++) {
		getline(fin, s);
		for (int j = 0; j < n; j++)
			pool[i][j] = s[j];
	}
	int x, y;
	pt >> x >> y >> xc >> yc;
	x--;
	y--;
	xc--;
	yc--;
	xb = x; yb = y;
	minPathCount = n*n;
	pool[x][y] = '+';
	Step(x, y, 1);
	pt << minPath;
}
