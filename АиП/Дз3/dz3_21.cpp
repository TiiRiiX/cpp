// dz3_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>


using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	double eps;
	double x;
	cout << "Eps=";
	cin >> eps;
	cout << "x=";
	cin >> x;
	double new_a = x;
	double sum = 0;
	int i = 1;
	while (new_a > eps){
		sum += new_a;
		new_a*= (x*x)/((i+1)*(i+2));
		i+=2;
	}
	cout << setprecision(15) << sum  << endl;
	return 0;
}

