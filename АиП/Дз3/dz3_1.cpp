// dz3_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int n;
	cin >> n;
	int sum = 1;
	int first_a = 1;
	for (int i = 2; i <= n; i ++){
		first_a = first_a + (2*i*i);
		sum += first_a;
		cout << first_a << " ";
	}
	cout << endl;
	cout << sum << endl;
	return 0;
}

