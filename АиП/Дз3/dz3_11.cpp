// dz3_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	int n;
	cout << "N=";
	cin >> n;
	double sum = 0.5;
	double first_a = 0.5;
	cout << "a1 = " << first_a;
	for (int i = 2; i <= n; i++){
		//cout << (2.0*i - 1.0)/(2.0*(2.0*i - 3.0)) << endl;
		first_a *= (2.0*i - 1.0)/(2.0*(2.0*i - 3.0));
		sum += first_a;
		cout << "a" << i << " = " << first_a << " ";
	}
	cout << endl;
	cout << "Sum=" << sum << endl;
	return 0;
}

