#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Add(int x, PNode &Head) {
	PNode NewNode;
	if (Head == NULL) {
		Head = new TNode;
		NewNode = Head;
		Head->Next = NULL;
	}
	else {
		NewNode = new TNode;
		NewNode->Next = Head;
		Head = NewNode;
	}
	NewNode->Data = x;
}

void Solve()
{
    Task("ListWork19");
	PNode Head = new TNode;
	int n;
	pt >> n;
	Head = NULL;
	for (int i = 0; i < n;i++) {
		int t;
		pt >> t;
		Add(t, Head);
	}
	pt << Head;
}
