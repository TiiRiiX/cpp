#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <iostream>
#include <fstream>
using namespace std;

int n, xc, yc, maxPath;
char **pool;

void Step(int x, int y, int depth) {
	if (x != 0 && x != n - 1 && y != 0 && y != n - 1) {
		if (x != xc || y != yc) {
			if (pool[x - 1][y] == ' ') { 
				pool[x - 1][y] = '+';
				Step(x - 1, y, depth + 1);
				pool[x - 1][y] = ' ';
			}
			if (pool[x + 1][y] == ' ') { 
				pool[x + 1][y] = '+';
				Step(x + 1, y, depth + 1);
				pool[x + 1][y] = ' ';
			}
			if (pool[x][y - 1] == ' ') {
				pool[x][y - 1] = '+';
				Step(x, y - 1, depth + 1);
				pool[x][y - 1] = ' ';
			}
			if (pool[x][y + 1] == ' ') {
				pool[x][y + 1] = '+';
				Step(x, y + 1, depth + 1);
				pool[x][y + 1] = ' ';
			}
		}
		else
			if (maxPath < depth)
				maxPath = depth;
	}
}

void Solve()
{
    Task("BackTrack11");
	string inputFile;
	string s;
	pt >> inputFile;
	pt >> n;
	ifstream fin(inputFile);
	pool = new char *[n];
	for (int i = 0; i < n; i++)
		pool[i] = new char[n];
	cin.ignore();
	for (int i = 0; i < n; i++) {
		getline(fin, s);
		for (int j = 0; j < n; j++)
			pool[i][j] = s[j];
	}
	int x, y;
	maxPath = 0;
	pt >> x >> y >> xc >> yc;
	x--;
	y--;
	xc--;
	yc--;
	pool[x][y] = '+';
	Step(x, y, 1);
	pt << maxPath;
}
