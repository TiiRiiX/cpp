#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void AddLast(PNode &Head, PNode &Tail, PNode NewNode) {
	NewNode->Next = NULL;
	NewNode->Prev = Tail;
	if (Tail)Tail->Next = NewNode;
	Tail = NewNode;
	if (Head == NULL) Tail = Head;
}

void Solve()
{
    Task("ListWork47");
	PNode Head, q, Tail;
	Tail = NULL;
	int n;
	pt >> Head >> q >> n;
	while (q->Next != Head)
		q = q->Next;
	for (int i = 0; i < n; i++) {
		PNode NewNode = new TNode;
		pt >> NewNode->Data;
		AddLast(Head, q, NewNode);
	}
	q->Next = Head;
	Head->Prev = q;
	pt << q;
}
