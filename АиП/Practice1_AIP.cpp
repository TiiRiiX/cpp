// Practice1_AIP.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

enum Color {Black, White, Red, Green, Blue, Yellow, Orange};
enum Type {Circle, Rect, Part};

union Properties
{
	short int raius;
	int a;
	float d;
};

struct Figura
{
	Color color;
	Properties properties;
	Type type;
};

Color chooseColor() {
	cout << "Введите цвет фиугры(1 - черный, 2 - белый, 3 - красный, 4 - зеленый, 5 - синий): ";
	int t;
	cin >> t;
	switch (t)
	{
	case 1:
		return Black;
	case 2:
		return White;
	case 3:
		return Red;
	case 4:
		return Green;
	case 5:
		return Blue;
	default:
		cout << "Что-то пошло не так, вводим стандартный белый" << endl;
		return White;
	}
}

Figura newFiguraFormConsole() {
	Figura newFigura;
	cout << "Какую фигуру будем вводить? (1 - круг, 2 - квадрат, 3 - отрезок): ";
	int choose;
	cin >> choose;
	switch (choose)
	{
	case 1:
		cout << "Введите радиус круга: ";
		cin >> newFigura.properties.raius;
		newFigura.color = chooseColor();
		newFigura.type = Circle;
		break;
	case 2:
		cout << "Введите сторону квадрата: ";
		cin >> newFigura.properties.a;
		newFigura.color = chooseColor();
		newFigura.type = Rect;
		break;
	case 3:
		cout << "Введите длинну отрезка: ";
		cin >> newFigura.properties.d;
		newFigura.color = chooseColor();
		newFigura.type = Part;
		break;
	default:
		cout << "Что-то пошло не так, я выбрал за вас квадрат" << endl << "Введите его сторону: ";
		cin >> newFigura.properties.a;
		newFigura.color = chooseColor();
		newFigura.type = Rect;
		break;
	}
	cout << "Фигура готова" << endl;
	return newFigura;
}

int main()
{

	setlocale(LC_ALL, "Russian");
	fstream bin("bin.txt", ios::binary | ios::in | ios::out);
	cout << "Количество фигур: ";
	int n;
	cin >> n;
	for (int i = 0; i < n; i++) {
		Figura t = newFiguraFormConsole();
		bin.write((char*)&t, sizeof(t));
	}
	bin.seekp(0, ios::beg);
	for (int i = 0; i < n; i++) {
		Figura t;
		bin.read((char*)&t, sizeof(t));
		string color;
		string type;
		string param;
		switch (t.color)
		{
			case Black:
				color = "Черный";
				break;
			case White:
				color = "Белый";
				break;
			case Red:
				color = "Красный";
				break;
			case Green:
				color = "Зеленый";
				break;
			case Yellow:
				color = "Желтый";
				break;
			case Orange:
				color = "Ораньжевый";
				break;
			case Blue:
				color = "Синий";
				break;
		}
		switch (t.type)
		{
		case Circle:
			type = "Круг";
			param = "Радиус = " + to_string(t.properties.raius);
			break;
		case Rect:
			type = "Квадрат";
			param = "Сторона = " + to_string(t.properties.a);
			break;
		case Part:
			type = "Отрезок";
			param = "Длина = " + to_string(t.properties.d);
			break;
		}
		cout << type << " " << param << " Цвет: " << color << endl;
	}
	bin.close();
    return 0;
}

