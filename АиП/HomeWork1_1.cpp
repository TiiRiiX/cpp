#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

struct sportman
{
	string surname, name;
	int first, second, three;
	int sum;
};

void bubbleSort(sportman* a, int n)
{
	sportman temp;
	bool exit = false;
	while (!exit)
	{
		exit = true;
		for (int i = 0; i < (n - 1); i++)
			if (a[i].sum < a[i + 1].sum)
			{
				temp = a[i];
				a[i] = a[i + 1];
				a[i + 1] = temp;
				exit = false;
			}
	}
}

void main()
{
	setlocale(LC_ALL, "Russian");
	ifstream fin("HomeWork1_1_input.txt");
	ofstream bout("HomeWork1_1_bin.txt", ios::binary);
	int n = 0;
	while (!fin.eof()) {
		sportman newMan;
		fin >> newMan.surname >> newMan.name >> newMan.first >> newMan.second >> newMan.three;
		newMan.sum = newMan.first + newMan.second + newMan.three;
		bout.write((char*)&newMan, sizeof(newMan));
		n++;
	}
	fin.close();
	bout.close();
	ifstream bin("HomeWork1_1_bin.txt", ios::binary);
	sportman *mas = new sportman[n];
	for(int i = 0; i < n; i++)
		bin.read((char*)&mas[i], sizeof(mas[i]));
	bubbleSort(mas, n);
	for (int i = 0; i < n; i++) {
		cout << mas[i].surname << " " << mas[i].name << " " << mas[i].first << " " << mas[i].second << " " << mas[i].three << " (" << mas[i].sum << ")" << endl;
	}
	
}