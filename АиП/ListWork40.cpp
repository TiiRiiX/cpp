#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;


void AddFirst(PNode &Head, PNode &Tail, PNode NewNode) {
	NewNode->Next = Head;
	NewNode->Prev = NULL;
	if (Head) Head->Prev = NewNode;
	Head = NewNode;
	if (Head == NULL) Head = Tail;
}

void AddLast(PNode &Head, PNode &Tail, PNode NewNode) {
	NewNode->Next = NULL;
	NewNode->Prev = Tail;
	if (Tail)Tail->Next = NewNode;
	Tail = NewNode;
	if (Head == NULL) Tail = Head;
}

void Solve()
{
    Task("ListWork40");
	PNode Head = new TNode;
	PNode Tail;
	Tail = NULL;
	int n;
	pt >> n;
	for (int i = 0; i < n;i++)
	{
		PNode NewNode = new TNode;
		pt >> NewNode->Data;
		AddLast(Head, Tail, NewNode);
	}
	PNode currPos = Tail;
	while (currPos->Prev != NULL) {
		currPos = currPos->Prev;
	}
	pt << currPos;
}
