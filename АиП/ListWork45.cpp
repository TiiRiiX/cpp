#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void ToFirst(PNode &L) {
	while (L->Prev != NULL)
		L = L->Prev;
}

void ToNext(PNode &L) {
	L = L->Next;
}

void SetData(PNode &L, int D) {
	L->Data = D;
}

bool isLast(PNode &L) {
	if (L ->Next == NULL)
		return true;
	else
		return false;
}

void Solve()
{
    Task("ListWork45");
	PNode Head;
	PNode Tail;
	PNode cur;
	pt >> Head;
	pt >> Tail;
	pt >> cur;
	ToFirst(cur);
	int p = 1;
	int count = 1;
	while (!isLast(cur)) {
		if (p == 1)
			SetData(cur, 0);
		p *= -1;
		count++;
		ToNext(cur);
	}
	if (p == 1)
		SetData(cur, 0);
	pt << count << Head << Tail << cur;
}
