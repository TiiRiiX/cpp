#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Print(PNode Tree) {
	if (!Tree) return;
	Print(Tree->Right);
	Print(Tree->Left);
	if (Tree->Left == NULL && Tree->Right == NULL)
		pt << Tree->Data;
}

void Solve()
{
    Task("TreeWork5");
	PNode Tree;
	pt >> Tree;
	Print(Tree);
}
