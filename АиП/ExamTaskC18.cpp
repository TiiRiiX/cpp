#include <windows.h>
#include <iostream>
#include <iomanip>
#pragma hdrstop
#include "pt4exam.h"


void Solve()
{
    Task("ExamTaskC18");
	int n, target;
	bool isGetClient = false;
	cin >> target >> n;
	int **mas = new int*[11];
	for (int i = 0; i < 11; i++) {
		mas[i] = new int[12];
		for (int j = 0; j < 12; j++)
			mas[i][j] = -1;
	}
	for (int i = 0; i < n; i++) {
		int kod, time, month, year;
		cin >> kod >> time >> month >> year;
		if (target == kod) {
			isGetClient = true;
			if (mas[year - 2000][month - 1] != -1)
				mas[year - 2000][month - 1] += time;
			else
				mas[year - 2000][month - 1] = time;
		}
	}
	if (isGetClient) {
		int **outmas = new int*[11];
		for (int i = 0; i < 11; i++) {
			outmas[i] = new int[2];
			outmas[i][0] == outmas[i][1] == -1;
			int count = 0;
			bool isPrint = false;
			for (int j = 0; j < 12; j++)
				if (mas[i][j] > 15) {
					count++;
					isPrint = true;
				}
				else if (mas[i][j] != -1)
					isPrint = true;
			if (isPrint) {
				outmas[i][0] = count;
				outmas[i][1] = i + 2000;
			}
		}
		for (int i = 1; i < 11; ++i)
		{
			for (int r = 0; r < 11 - i; r++)
			{
				if (outmas[r][0] < outmas[r + 1][0])
				{
					// Обмен местами
					int* temp = outmas[r];
					outmas[r] = outmas[r + 1];
					outmas[r + 1] = temp;
				}
				else if (outmas[r][0] == outmas[r + 1][0]) {
					if (outmas[r][1] > outmas[r + 1][1]) {
						int* temp = outmas[r];
						outmas[r] = outmas[r + 1];
						outmas[r + 1] = temp;
					}
				}
			}
		}
		for (int i = 0; i < 11;i++)
			if (outmas[i][0] >= 0)
				cout << outmas[i][0] << " " << outmas[i][1] << endl;
	}
	else
		cout << "Нет данных";
}
