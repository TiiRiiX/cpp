// dz4_7b.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

struct drob
{
	int up;
	int down;
};

int NOD(int a, int b){
	while (a != b)
		if (a > b) a -= b;
		else b -= a;
	return a;
}

drob plus(drob a, drob b){
	drob outDrob;
	int tempDown = a.down*b.down;
	int tempUp = a.up*b.down + b.up*a.down;
	outDrob.up = tempUp/NOD(tempUp, tempDown);
	outDrob.down = tempDown/NOD(tempUp, tempDown);
	return outDrob;
}

drob multi(drob a, drob b){
	drob outDrob;
	int tempUp = a.up*b.up;
	int tempDown = a.down*b.down;
	outDrob.up = tempUp/NOD(tempUp, tempDown);
	outDrob.down = tempDown/NOD(tempUp, tempDown);
	return outDrob;
}

int _tmain(int argc, _TCHAR* argv[])
{
	drob a,b,c,d;
	cout << "(a/b + c/d + k/m) * e/f" << endl;
	cout << "a,b,c,d,k,m,e,f : ";
	cin >> a.up >> a.down >> b.up >> b.down >> c.up >> c.down >> d.up >> d.down;
	cout << "(" << a.up << "/" << a.down << " + " << b.up << "/" << b.down << " + " << c.up << "/" << c.down << ")" << " * " << d.up << "/" << d.down << endl;
	drob plusOne = plus(a,b);
	drob plusTwo = plus(plusOne,c);
	drob solveDrob = multi(plusTwo,d);
	cout << solveDrob.up << "/" << solveDrob.down << endl;
	return 0;
}

