#include <windows.h>
#pragma hdrstop
#include "pt4.h"
#include <fstream>
#include <math.h> 
using namespace std;
#include <iomanip>
#include <cmath>

double Hypo(double a, double b){
	return sqrt(a*a+b*b);
}

double P(double a, double b){
	return a + b + sqrt(a*a+b*b);
}

double S(double a, double b){
	return ((b*a)/2.0);
}

void Solve()
{
    Task("Tri2");
	string inputFile, outputFile;
	pt >> inputFile >> outputFile;
	ifstream fin(inputFile);
	ofstream fout(outputFile);
	int N;
	fin >> N;
	for (int i = 0; i < N; i++){
		double a,b;
		fin >> a >> b;
		fout << setprecision(2) << fixed << a << " " << b << " " << Hypo(a,b) << " " << P(a,b) << " " << S(a,b) << endl;
	}
	fin.close();
	fout.close();
}
