#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

int Converter(int main){
	int solv = 0;
	int s10 = 1;
	while (main > 0){
		solv = (main % 9)*s10 + solv;
		s10 *= 10;
        main = main / 9;
	}
	return solv;
}

void Solve()
{
    Task("Tri10");
	int inp;
	do{
	pt >> inp;
	pt << Converter(inp);
	}
	while( inp != 0);
}
