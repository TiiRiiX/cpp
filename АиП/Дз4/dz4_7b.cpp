// dz4_7b.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

int _tmain(int argc, _TCHAR* argv[])
{
	double x;
	double eps;
	cout << "Ln(x - 1)" << endl;
	cout << "X=";
	cin >> x;
	cout << "Eps=";
	cin >> eps;
	double new_a;
	new_a = x;
	double sum = -x;
	for(int i = 2; new_a > eps; i++){
		new_a = new_a*((x*(i - 1))/i);
		sum -= new_a;
		//cout << new_a << endl;
	}
	cout << endl;
	cout << "Sum=" << sum << endl;
	cout << "Log=" << log(1 - x) << endl;
	return 0;
}

