#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

int sumNumbers(int a){
	int sum = 0;
	while (a > 0){
		sum += a % 10;
		a /= 10;
	}
	return sum;
}

void Solve()
{
    Task("Tri21");
	int x;
	pt >> x;
	int countIter = 0;
	while (x > 0){
		x -= sumNumbers(x);
		countIter++;
	}
	pt << countIter;
}
