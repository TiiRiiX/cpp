// Individual2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

struct  Node
{
	int x; //поле данных
	Node *next; //ссылка на следующий элемент
};

typedef Node *PNode;

void Add(int x, PNode &Head, PNode &MyNode)
{
	PNode Temp;
	if (Head == NULL) //Если очередь пуста, создаем ее сначала
	{
		Head = new Node; 
		MyNode = Head; //Последний элемент обозначаем, как "голово"
		Head->next = NULL; //Ссылку на следующий элемент выставляем на NULL
	}
	else
	{
		Temp = new Node; 
		MyNode->next = Temp; //Создаем новый последний элемент
		MyNode = Temp; //Переприсваем последний элемент только что созданным
		MyNode->next = NULL; //Ссылка на следующий элемент выставляем на NULL
	}
	MyNode->x = x; //В поле данных заносим число
}

void ClearNode(PNode &Head)
{
	PNode MyNode;
	while (Head != NULL) //Пока очереди не будет существовать
	{
		MyNode = Head->next; //Указатель на текущий элемент переносим на следующий
		delete Head;//Удаляем указатель на начало очереди
		Head = MyNode;//Переносим начало очереди на первый существующий
	}
}

void DeleteHead(PNode &Head)
{
	Head = Head->next;//Переприсваиваем указатель на следующий за ним
}

int main()
{
	PNode Head_x2, Head_x3, Head_x5, Nodex2, Nodex3, Nodex5;
	Head_x2 = NULL;
	Head_x3 = NULL;
	Head_x5 = NULL;
	int n;
	cin >> n; //Вводим количество необходимых элементов
	Add(2, Head_x2, Nodex2);//Добавляем в очереди минимальные элементы
	Add(3, Head_x3, Nodex3);
	Add(5, Head_x5, Nodex5);
	int lastA = 0;//Объявляем последний напечатанный
	for (int i = 0; i < n; i++){
		int minElem = Head_x2->x;//Находим минимальный элемент
		int chose = 2;//и запоминаем очередь, в которой он находится
		if (minElem > Head_x3->x) { minElem = Head_x3->x; chose = 3; }
		if (minElem > Head_x5->x) { minElem = Head_x5->x; chose = 5; }
		if (lastA != minElem) {			//Если последний напечатанный элемент
			lastA = minElem; //не совпадает с новым найденным минимумом
			cout << minElem << " ";//то печатаем его и запоминаем 
		}
		else {
			n++; //Иначе увеличиваем число итераций цикла
		}
		Add(minElem * 2, Head_x2->next, Nodex2);//Добавляем в очереди
		Add(minElem * 3, Head_x3->next, Nodex3);//найденный минимальный элемент
		Add(minElem * 5, Head_x5->next, Nodex5);//умноженный на 2,3 и 5
		switch (chose)//Удаляем минимальный элемент из очереди
		{			//В которой он находится
		case 2:
			DeleteHead(Head_x2);
			break;
		case 3:
			DeleteHead(Head_x3);
			break;
		case 5:
			DeleteHead(Head_x5);
			break;
		}
	}
	cout << endl;
	ClearNode(Head_x2);//Чистим память
	ClearNode(Head_x3);
	ClearNode(Head_x5);
    return 0;
}

