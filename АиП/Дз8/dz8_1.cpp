// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace std;

void printMatrix(float **matrix, int n){
	for (int x = 0; x < n; x++){
		for (int y = 0; y < n; y++)
			cout << matrix[x][y] << " ";
		cout << endl;
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	ifstream fin("in.txt");
	int n;
	fin >> n;
	float **a = new float*[n];
	for (int i = 0; i < n; i++)
		a[i] = new float[n];
	for (int x = 0; x < n; x++)
		for (int y = 0; y < n; y++)
			fin >> a[x][y];
	printMatrix(a, n);
	int k;
	cout << "K=";
	cin >> k;
	float diagonal = a[k-1][k-1];
	for (int i = 0; i < n; i++)
		a[k-1][i] /= diagonal;
	printMatrix(a, n);
	for (int i = 0; i < n; i++)
		delete[] a[i];
	delete[] a;
	fin.close();
	return 0;
}

