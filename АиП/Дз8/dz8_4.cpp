// dz5_1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <climits>

using namespace std;

void printMatrix(float **matrix, int n, int m){
	for (int x = 0; x < n; x++){
		for (int y = 0; y < m; y++)
			cout << matrix[x][y] << " ";
		cout << endl;
	}
}

struct number{
	float a;
	int x;
	int y;
};

int _tmain(int argc, _TCHAR* argv[])
{
	ifstream fin("in.txt");
	int n,m;
	fin >> n >> m;
	float **a = new float*[n];
	number *minTable = new number[n];
	for (int i = 0; i < n; i++)
		a[i] = new float[m];
	for (int x = 0; x < n; x++){
		number min;
		min.a = INT_MAX;
		for (int y = 0; y < m; y++){
			fin >> a[x][y];
			if (min.a > a[x][y]){
				min.a = a[x][y];
				min.x = x;
				min.y = y;
			}
		}
		minTable[x] = min;
	}
	number max;
	max.a = -INT_MAX;
	for (int i = 0; i < n; i++)
		if(minTable[i].a > max.a)
			max = minTable[i];
	printMatrix(a, n, m);
	cout << "Max from min = " << max.a << " X: " << max.x << " Y: " << max.y << endl;
	for (int i = 0; i < n; i++)
		delete[] a[i];
	delete[] a;
	delete[] minTable;
	fin.close();
	return 0;
}

