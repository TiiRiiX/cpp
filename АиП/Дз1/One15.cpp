#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

#include <iostream>

void Solve()
{
    Task("One15");

	int time;
	int pathFirst;
	int procentPathSecond;
	float speedFirst;
	float speedSecond;
	pt >> time >> pathFirst >> procentPathSecond;
	speedFirst = pathFirst/double(time);
	speedSecond = (double(pathFirst)/(100 + procentPathSecond))*100/double(time);
	pt << speedFirst << speedSecond;
}
