#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Solve()
{
    Task("One29");
	double a;
	pt >> a;
	double b = a*a;
	double c = b*b;
	double d = c*c;
	double e = d*d;
	double solv = e*b;
	pt << solv;
}
