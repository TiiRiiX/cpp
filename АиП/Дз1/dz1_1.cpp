// task1.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>

using namespace std;


int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(LC_ALL, "Russian");

	float sum;
	int days;
	int procent;
	cout << "------------------------------------------" << endl;
	cout << "Вычисление дохода по вкладу." << endl;
	cout << "Введите исходные данные:" << endl;
	cout << "Величина вклада (руб.) > ";
	cin >> sum;
	cout << "Срок вклада (дней) > ";
	cin >> days;
	cout << "Процентная  ставка (% годовых) > ";
	cin >> procent;
	cout << "------------------------------------------" << endl;
	float profit = ( sum * ( procent / 100.0 ) / 365.0 ) * days;
	cout << "Доход: " << setprecision(2) << fixed << profit << " руб." << endl;
	cout << "Сумма по окончанию срока  вклада: " << profit + sum << " руб." << endl;
	return 0;
}

