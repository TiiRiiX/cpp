#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void Solve()
{
    Task("One42");
	int main;
	pt >> main;
	int numbers[6];
	for (int i = 0; i < 6; i++){
		numbers[i] = main % 10;
		main /= 10;
	}
	pt << (numbers[0] * numbers[1] * numbers[2] == numbers[3] + numbers[4] + numbers[5]);
}
