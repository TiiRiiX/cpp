#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void AddAfter(PNode p, PNode NewNode) {
	NewNode->Next = p->Next;
	p->Next = NewNode;
}

void AddBefore(PNode &Head, PNode p, PNode NewNode) {
	PNode q = Head;
	if (Head == p) {
		NewNode->Next = Head;
		Head = NewNode;
		return;
	}
	while (q && q->Next != p)
		q = q->Next;
	if (q)
		AddAfter(q, NewNode);
}

void Solve()
{
    Task("ListWork33");
	int x, n;
	PNode Head;
	pt >> x >> n >> Head;
	PNode cur = Head;
	for (int i = 1; i < n;i++)
		cur = cur->Next;
	PNode NewNode = new TNode;
	NewNode->Data = x;
	AddBefore(Head, cur, NewNode);
	pt << cur;
}
