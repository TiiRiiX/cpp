// HomeWork1_2.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	int n;
	cin >> n;
	float t;
	fstream bin("HomeWork1_2_bin.txt", ios::binary | ios::in | ios::out);
	for (int i = 0; i < n; i++) {
		cin >> t;
		bin.write((char*)&t, sizeof(t));
	}
	for (int i = 0; i < n; i+=2)
		if (i < n - 1) {
			float t1,t2;
			bin.seekp(i*sizeof(float),ios::beg);
			bin.read((char*)&t1, sizeof(t1));
			bin.seekp((i + 1) * sizeof(float), ios::beg);
			bin.read((char*)&t2, sizeof(t2));

			bin.seekg(i * sizeof(float), ios::beg);
			bin.write((char*)&t2, sizeof(t2));
			bin.seekg((i + 1) * sizeof(float), ios::beg);
			bin.write((char*)&t1, sizeof(t1));
		}
	cout << endl;
	bin.seekp(0, ios::beg);
	for (int i = 0; i < n; i++) {
		bin.read((char*)&t, sizeof(t));
		cout << t << endl;
	}
	bin.close();
	return 0;

}