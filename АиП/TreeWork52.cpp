#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

int *depths = new int[10];
int maxDepth = 0;

void checkDepth(PNode Tree, int newDepth) {
	if (!Tree) return;
	depths[newDepth]++;
	if (maxDepth < newDepth)
		maxDepth = newDepth;
	checkDepth(Tree->Left, newDepth + 1);
	checkDepth(Tree->Right, newDepth + 1);
}

void Solve()
{
    Task("TreeWork52");
	PNode Tree;
	pt >> Tree;
	for (int i = 0; i < 10; i++)
		depths[i] = 0;
	checkDepth(Tree, 0);
	bool isPerfect = true;
	for (int i = 0, two = 1; i < maxDepth; i++, two *= 2)
		if (depths[i] != two)
			isPerfect = false;
	pt << isPerfect;
}
