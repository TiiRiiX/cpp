#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

int *depths = new int[10];

void checkDepth(PNode Tree, int newDepth) {
	if (!Tree) return;
	depths[newDepth]++;
	checkDepth(Tree->Left, newDepth + 1);
	checkDepth(Tree->Right, newDepth + 1);
}

void Solve()
{
    Task("TreeWork21");
	PNode Tree;
	int k;
	pt >> k >> Tree;
	for (int i = 0; i < 10; i++)
		depths[i] = 0;
	checkDepth(Tree, 0);
	pt << depths[k];
}
