#include <windows.h>
#pragma hdrstop
#include "pt4.h"
using namespace std;

void AddLast(PNode &Head, PNode &Tail, PNode NewNode) {
	NewNode->Next = NULL;
	NewNode->Prev = Tail;
	if (Tail)Tail->Next = NewNode;
	Tail = NewNode;
	if (Head == NULL) Tail = Head;
}

bool isLast(PNode &L) {
	if (L->Next == NULL)
		return true;
	else
		return false;
}

void DeleteNode(PNode &Head, PNode &Tail, PNode OldNode) {
	if (Head == OldNode) {
		Head = OldNode->Next;
		if (Head) Head->Prev = NULL;
		else Tail = NULL;
	}
	else {
		if (OldNode->Prev)
			OldNode->Prev->Next = OldNode->Next;
		if (OldNode->Next)
			OldNode->Next->Prev = OldNode->Prev;
		else {
			Tail = OldNode->Prev;
			Tail->Next = NULL;
		}
	}
	delete OldNode;
}

bool checkSimple(int a) {
	for (int i = 2; i < a;i++)
		if (a % i == 0)
			return false;
	return true;
}

void Solve()
{
	Task("ListWork74");
	PNode Head = new TNode;
	PNode Tail;
	Tail = NULL;
	int n;
	pt >> n;
	for (int i = 0; i < n;i++)
	{
		PNode NewNode = new TNode;
		pt >> NewNode->Data;
		AddLast(Head, Tail, NewNode);
	}
	PNode cur = Tail;
	for (int i = 0; i < n;i++) {
		if (!checkSimple(cur->Data)) {
			PNode OldNode = cur;
			DeleteNode(Head, Tail, OldNode);
		}
		cur = cur->Prev;
	}
	PNode currPos = Tail;
	while (currPos->Prev != NULL) {
		currPos = currPos->Prev;
	}
	pt << currPos;
}